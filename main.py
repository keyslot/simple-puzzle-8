'''
\*****************   *  SIMPLE PUZZLE 8 *  ***********************/
Ortega Vallejo Raúl Antonio
Algoritmo: A* con Halming
\****************************************************************\*
''' 
# Bibliotecas requeridas:
from collections import OrderedDict # Diccionario ordenado durante las iteraciones
from time import sleep # Visualizar mejor los estados
from copy import copy # Copiar tableros para crear los nodos
from random import choice # Para tableros aleatorios

# Clase Texto:  Se utiliza para dar tono al texto en terminal
class Texto:
     def __init__(self):
         self.colores = {
                 "bold":'\033[1m',  # Default
                 "error":'\033[93m', # Errores
                 "amarillo":'\x1b[1;33;40m',# Izquierda
                 "azul":'\x1b[1;34;40m', # Abajo
                 "verde":'\x1b[1;32;40m', # Arriba
                 "rojo":'\x1b[1;31;40m', # Derecha 
                 "casilla":'\x1b[1;37;40m' # Número
                 }
      
     def color(self,
             cadena, # Cadena a "Colorear"
             tipo = "bold" # Por default, se resalta 
             ): 

         inicio = self.colores[tipo] 
         fin = '\033[0m' 
         return inicio + cadena + fin

     def clear(self):
         self.colores.clear()
         del self

# Clase Nodo: Permite controlar el origen de un estado y evaluarlo
class Nodo:
   
    def __init__ (self,
                 tablero, # Origen del nodo
                 direccion # El movimiento que lo originó 
            ):

        self.tablero = tablero # Objeto del tipo Tablero()
        self.padre = tablero.estadoActual # Apuntamos al padre del Nodo 
        self.direccion = direccion # Direccion, eje: "Arriba|Abajo|Izquierda|Derecha"
        self.h = 0 # Valor de la Heuristica
        self.funcionEvaluacion() # Evaluamos al nodo 
    
    def muestraTablero(self):
        '''
        Imprime el movimiento, Imprime el Tablero del Nodo
        '''
        print (self.direccion) 
        self.tablero.muestraTablero() # Metodo del objeto tipo Tablero()

    def estado (self):
        '''
        Estado del Nodo:
        Regresamos el movimiento del nodo (el tablero)
        '''
        return str(self.tablero.movimiento) #Ultimo movimiento del objeto tipo Tablero()
    
    def funcionEvaluacion(self):
        '''
        Evaluacion  heuristica
        '''
        distancia = self.tablero.movimiento # Tablero del nodo
        objetivo  = self.tablero.tableroFinal # Tablero final
        # Evaluamos la distancia que tomo crear el nodo

        # Si existe antecesor del nodo, se suma la distancia
        if self.padre: 
            self.g = self.padre.g + 1

        # El nodo es raiz
        else:  
            self.g = 0 

        # Evaluamos casillas fuera de lugar (Halmmin) 
        # Sumatoria Halmming + distancia de raiz al nodo = Costo del Nodo
        for i in range (0, len(objetivo)): 
            if distancia[i]!=0 and distancia [i] != objetivo[i] : 
                self.h += 1
                self.h += self.g # Esto evita empates
        # self.h += self.g

# Clase Tablero controla los movimientos, generación de nuevos nodos.
class Tablero:
    def __init__ (self,
                  configuracion # Configuración de la Partida (Puzzle8)
                  ):

        self.posiblesMovimientos = None # Movimientos posibles aun no evaluados en el Tablero,
        self.tablero = self.movimiento = configuracion["Inicial"] # Configuracion: Inicial -> Estado Inicial
        self.tableroFinal = configuracion["Final"] # Configuracion: Final -> Estado Final
        self.tiempo = configuracion["Lag"] # Tiempo de espera imprimir en Pantalla
        self.tamTablero = len(self.tableroFinal) # Tamaño del Tablero 
        self.estadoActual = None # Apuntador del nodo actual
        self.totalNodos = 0   # Nodos expandidos
      
    def muestraTablero(self,
                       tablero=None, #Variable para probar la función
                       ):
        if not tablero: tablero = self.movimiento 
        # Imprimimos el tablero 
        for i in range(0, len(tablero),3): # 3 columnas
            print (tablero[i:i+3])    # 3 filas
        print()

    def regresaCasilla (self,index):
        " Evalua un index, y regresa elemento de dicho. Esto evita salir del tablero "
        return self.tablero[index] if 0 <= index <= self.tamTablero-1 else None
        
    def regresaDireccion(self,eleccion):
        " Regresa una direccio si existe en los movimientos"
        if eleccion in self.posiblesMovimientos.values():
            for direccion, casilla in self.posiblesMovimientos.items(): 
                if casilla == eleccion:
                    return direccion
        return None

    def evaluaPosiblesMovimientos (self):
        """
        Evaluar las adyacencias de la casilla nula
        """
        self.posiblesMovimientos = OrderedDict() # Diccionario Ordenado 
        t = Texto() 
        tablero = self.tablero  # Apuntador al tablero
        vacio = tablero.index(0) # Regresa la posicion de la casilla vacia
        derecha = self.regresaCasilla (vacio + 1) # Adyacencia derecha de casilla vacia
        izquierda = self.regresaCasilla (vacio - 1) # Adyacencia izquierda de casilla vacia
        abajo  = self.regresaCasilla (vacio  + 3)  # Adyacencia debajo de casilla vacia
        arriba  = self.regresaCasilla (vacio - 3) # Adyacencia arriba de casilla vacia

        "Si en adyacencia hay un elemento y esta está en la misma fila:"
        if derecha and tablero.index(derecha) % 3: 
            self.posiblesMovimientos[t.color("Derecha","rojo")] = derecha
        if izquierda and vacio % 3:
            self.posiblesMovimientos[t.color("Izquierda","amarillo")] = izquierda
        "Si existe un elemento en la adyacncia:"
        if abajo:
            self.posiblesMovimientos[t.color("Abajo","azul")] = abajo
        if arriba:
            self.posiblesMovimientos[t.color("Arriba","verde")] =arriba

    def mueveCasilla (self, 
            casilla # Casilla/Bloque a mover, de tipo int 
            ):
        "Si es posible mover la casilla:"
        if casilla in self.posiblesMovimientos.values():
            movimiento = self.tablero [:] # Copia del tablero para moverlp
            indexCasilla = movimiento.index(casilla) 
            indexVacio = movimiento.index(0) # Regresa posición de la casilla vacia (0)
            "Intercambio de casillas"
            movimiento[indexCasilla] = 0
            movimiento[indexVacio] = casilla
            self.movimiento = movimiento # Actualizamos el atributo movimiento
            return movimiento # Regresamos el movimiento, por si se llaga a utilizar
        

    def muestraSuegerencia(self):
        """
        Esta función sugiere un movimiento al usuario
        """
        tablero = self.tablero [:] # Copia del Tablero Actual 
        auxiliar = self.estadoActual # Apuntador auxiliar del estado actual
        self.estadoActual = None # Apuntamos a Nulo para Resolver en Modo Inteligente
        mejorOpciones = self.resuelveInteligente(True) # Resolviendo el Tablero
        self.tablero = tablero # Regresamos el Tablero a la Configuracion Original
        self.estadoActual = auxiliar # Apuntamos a la copia auxiliar del nodo actual
        
        "Necesitamos el mejor movimiento apartir del tablero del usuario"
        mejorOpcion = mejorOpciones[-2] # -1 -> Estado Inicial       
        del mejorOpciones # Libera memoria
        self.evaluaPosiblesMovimientos() # Reparamos las evaluaciones de los posibles movimientos
        direccion = mejorOpcion.direccion # Regresamos la mejor direccion
    
        "Mostramos la sugerencia"
        print("Sugiero  mover %s: [%s]"%(direccion,self.posiblesMovimientos[direccion]))
        acepta = input ("¿Aceptar? s/n\n_") # Preguntamos si desea aceptarla
        
        if acepta == "s": # Acepto el usuario...
            return self.posiblesMovimientos[direccion] #Regresa la sugerencia para ser agregada
    
        self.movimiento = self.tablero # Regresamos el movimiento del tablero como estaba
        return None # Regresamos null


    def eligeNodo(self,
                  arbol, # Lista de nodos expandidos
                  estadosVisitados # Lista de estados (nodos) visitados 
                  ):
        """
        Esta funcion permite al usuario elegir los nodos a generar. 
        Es posible agregar estados repetidos en el modo manual
        """
        t = Texto() 
        eleccion = None # Eleccion del usuario
        self.evaluaPosiblesMovimientos() # Evaluamos los posibles movimientos
        while 1: # Mientras el usuario no eliga algo valido
            
            "Mostramos los posiblesMovimientos"
            for direccion,casilla in self.posiblesMovimientos.items():
                numero = "["+str(casilla)+"]"
                print ("%s %s"%(t.color(numero,"casilla"),direccion))
            
            eleccion = input ( t.color ("\
                    \n------------------------------\
                    \n[s] Solicita sugerencia\
                    \n[t] Que termine la computadora \
                    \n[x] Cancelar el Juego \
                    \n------------------------------\
                    \n_") )
         
            if eleccion == "s":  eleccion = self.muestraSuegerencia () # Solicita sugerencia
            elif eleccion == "t" or eleccion == "x":  
                 if eleccion == "t":  # Que termine la computadora
                    self.estadoActual = None
                    self.resuelveInteligente() # Modo inteligente
                 self.estadoActual = None # Apuntamos a null el estado actual
                 arbol.clear()  # Limpiamos el arbol generado por usuario
                 return arbol,estadosVisitados
            else:
                try: eleccion = int(eleccion) 
                except: pass

            if eleccion and eleccion in self.posiblesMovimientos.values(): 
                break # Si el usuario elige algo valido
            self.muestraTablero() # Vuelve a mostrar el tablero
        
        direccion = self.regresaDireccion(eleccion) # Direccion seleccionada (Eje:Arriba|Abajo...)
        self.mueveCasilla(eleccion) # Casilla/Ellección seleccionada
        "Creamos el nodo"
        nuevoNodo = Nodo (copy(self),direccion) 
        estado = nuevoNodo.estado() # Estado del nodo
        
        "Aunque es posible expandir nodos repetidos, notificamos al usuario"
        if estado not in estadosVisitados:
            estadosVisitados.add(estado) # Agregamos el estado, si no ha sido visitado
        else: print (t.color(">>> Upss, ya se repitio el movimiento","error"))
        
        arbol.insert(0,nuevoNodo) # Agregamos el nodo al arbol 
        return arbol,estadosVisitados # Regresamos el arbol y los estados visitados

        
    def resuelveManual (self):
        """
        Resuelve el Tablero, por el usuario
        """
        t = Texto()
        arbol = [] # Lista de nodos a expandir
        raiz = Nodo(copy(self), t.color("Estado Inicial")) # Creamos la raíz
        t.clear() 
        arbol.append(raiz) # Agregamos la raiz
        estadosVisitados = set() # Evitamos la repetición de estados -> Propiedades de conjuntos
        estadosVisitados.add(raiz.estado())
        "Expandimos los nodos"
        while not self.resuelto() and arbol:
            nodo = arbol.pop(0) # Sacamos el mejor primero
            self.estadoActual  = nodo # Apuntamos al hijo 
            self.tablero = nodo.tablero.movimiento
            if self.resuelto(): break # Encontramos el objetivo
            self.muestraTablero() 
            self.totalNodos +=1
            "Pedimos al usuario elegir el nodo a expandir"
            arbol, estadosVisitados = self.eligeNodo(arbol,estadosVisitados)

        estadosVisitados.clear()
        arbol.clear()
        # Solucion creada por el usuario
        self.muestraSolucion()

    def expandeNodos(self,
                     arbol, # Lista de nodos expandidos
                     estadosVisitados # Lista de estados (nodos) visitados 
                     ):
        """
        Esta funcion evalua las adyacencias y genera nodos
        """
        self.evaluaPosiblesMovimientos() # Evalua las adyacencias
        for direccion,casilla in self.posiblesMovimientos.items():
            # print ("Moviendo ",direccion,casilla)
            self.mueveCasilla(casilla) # Mueve el movimiento del tablero
            nuevoNodo = Nodo (copy(self),direccion) # Crea un nodo del objeto actual y su origen
            estado = nuevoNodo.estado() # Apuntamos al estado del nodo
            "Agreamos los nodos mientras no se repitan"
            if estado not in estadosVisitados:
                estadosVisitados.add(estado)
                arbol.append(nuevoNodo)
            del nuevoNodo # Libera mem
        "Ordenamos el arbol acorde a la funcion evaluacion (heuristica)"
        arbol = sorted(arbol, key=lambda nodo:nodo.h) 
        return arbol,estadosVisitados # Regresamos arbol con nodos visitados

    def resuelveInteligente(self,
                            Sugiere=False #Parametro para regresar sugerencias
                            ):
        """
        Esta funcion resuelve el tablero, modo Inteligente
        """
        t = Texto()
        arbol = []
        raiz = Nodo(copy(self), t.color("Estado Inicial"))
        t.clear()
        arbol.append(raiz)
        estadosVisitados = set()
        estadosVisitados.add(raiz.estado())
        while not self.resuelto() and arbol:
            nodo = arbol.pop(0) # Sacamos el mejor primero
            self.estadoActual  = nodo
            self.tablero = nodo.tablero.movimiento
            if self.resuelto(): break
            self.totalNodos +=1
            arbol, estadosVisitados = self.expandeNodos(arbol,estadosVisitados)
        # Limpieza 
        estadosVisitados.clear()
        arbol.clear()
        return self.muestraSolucion(Sugiere)

       
    def resuelto (self):
        # Tablero Final
        return True if self.tablero == self.tableroFinal else False

    def muestraSolucion (self,
                         Sugerencia=False #Parametro para mostrar sugerencia
                         ):
        """
        Esta funcion muestra la solucción/camino optimo, si es que existe
        """
        if self.estadoActual:
            if self.resuelto():
                t = Texto()
                " Si no es sugerencia, Termino el juego"
                if not Sugerencia: print (t.color("Termino el juego"))

                ptr = self.estadoActual # Apuntamos al ultimo nodo generado (actual)
                mejorCamino = [] # Lista de caminos
                pasos = 0
                while ptr: # Caminamos hacia atras, hacía la raiz
                    mejorCamino.append(ptr) # Agregamos el nodo al camino
                    swp = ptr.padre # Apuntamos al padre
                    del ptr  # Limpieza
                    ptr = swp  # Intercambio

                "Regresamos el mejorCamino para ser sugerido"
                if Sugerencia: return mejorCamino

                "Mostrar el mejor camino:"
                while mejorCamino: 
                    sleep(self.tiempo)
                    camino  = mejorCamino.pop()
                    camino.muestraTablero()
                    pasos +=1

                pasos -=1 # Eltotal menos la Raiz
                print (pasos,"Pasos")
                #print (self.totalNodos,"Nodos expandidos")
                t.clear()

            else: # No se encotro la solución...
                t = Texto()
                print (t.color("Error, fallo la heuristica","error"))
                t.clear()
        return None



    def aleatorio(self,
                  ciclos #Numero de iteraciones/elecciones aleatorias
                 ):
        """
        Esta funcion itera cierto no. para elegir aleatoriamente un EstadoInicial
        Util para configurar la partida
        """
        self.movimiento = self.tablero = self.tableroFinal [:] # Semilla
        lista = [] #Lista de posibles partidas
        repetidos = set() # Estados visitados

        for i in range (0,ciclos):
            self.evaluaPosiblesMovimientos()
            "Evaluamos los posiblesMovimientos"
            for _,casilla in self.posiblesMovimientos.items():
                movimiento = self.mueveCasilla(casilla)
                estado = str(movimiento)
                if estado not in repetidos:
                    lista.append(movimiento)
                    repetidos.add(estado)
            "Elegimos aleatoriamente un tablero" 
            self.tablero = choice(lista) # Apuntamos al tablero elegido aleatoriamente

        repetidos.clear()
        lista.clear() 
        return self.tablero # Regresamos la partida 


class Puzzle:
    def __init__(self):
        " Preconfiguramos el tablero ... "
        self.configuracion = { 
                        "Inicial":None, #Puede especificarse | None => Aleaotorio
                        "Final"  :[1,2,3,4,5,6,7,8,0],# Estado Final
                        "Lag": 2# Tiempo de espera tras imprimir los estados
                        } 

    def configurar(self):
        "Configuramos el Tablero Inicial"
        if not self.configuracion["Inicial"]:
            ciclos = 10000
            partidaAleatoria = Tablero(self.configuracion).aleatorio(ciclos) # Solo necesitamos la partida
            self.configuracion["Inicial"] = partidaAleatoria
        print ("\nConfiguracion %s\n"%self.configuracion) # Imprime la configuracion
        sleep(self.configuracion["Lag"]) 
  

    def iniciar (self):
        "Configuramos el menu"
        t = Texto()
        menu = {1:["Quiero Jugar", self.modoManual],# KEY, [Descripción , Funcion]
                2:["Que la maquina juege sola", self.modoInteligente],
                3:["Salir", exit]}
        while 1:
            print (t.color(">>>>>>  PUZZLE8 <<<<<<< ")+"\n")
            print (t.color("------ M E N U ------"))
            for tecla,funcion in menu.items(): # Imprime el Menú
                print ("[%s] %s"%(tecla,funcion[0])) # Muestra las teclas
            try: 
                eleccion = int(input("_"))
                if eleccion in menu:
                    print (">>> %s...\n"%t.color(menu[eleccion][0]))
                    menu[eleccion][1]() # Ejecuta la función seleccionada del menú
            except Exception as e: print (e)


    def modoManual (self):
        "JUGAR MODO MANUAL"
        self.configurar() # Configura partidas aleatorias
        self.tablero = Tablero (self.configuracion)
        self.tablero.resuelveManual()

    def modoInteligente (self):
        "JUGAR MODO INTELIGENTE"
        self.configurar() # Configura partidas aleatorias
        self.tablero = Tablero (self.configuracion)
        self.tablero.resuelveInteligente()

"Iniciamos el Juego"
juego = Puzzle()
juego.iniciar()

